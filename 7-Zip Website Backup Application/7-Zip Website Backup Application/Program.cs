﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace _7_Zip_Website_Backup_Application
{
    class Program
    {
        static void Main(string[] args)
        {
            string _7ZipPathToExe;
            string sourcePath;
            string zipName;
            string websiteName;
            string parentDirectory;
            string archiveArguments;
            char[] charsToTrim = { '"' };

            //
            // Get 7-Zip path.
            //
            _7ZipPathToExe = string.Empty;
            if (_7ZipExe == "False")
            {
                Console.WriteLine("I could not find 7-Zip! Please tell me where it is:");
                _7ZipPathToExe = Console.ReadLine().Trim(charsToTrim);
            }
            else
            {
                Console.WriteLine("I found 7-Zip! \"" + _7ZipExe + "\"");
                Console.Write("Is this correct (Yes/No)? ");
                while (true)
                {
                    ConsoleKeyInfo result = Console.ReadKey(true);
                    if ((result.KeyChar.ToString() == "Y") || (result.KeyChar.ToString() == "y"))
                    {
                        _7ZipPathToExe = _7ZipExe;
                        Console.WriteLine("Okay, let's continue!");
                        break;
                    }
                    else if ((result.KeyChar.ToString() == "N") || (result.KeyChar.ToString() == "n"))
                    {
                        Console.Write("\n\nPoint me to the 7-Zip executable: ");
                        _7ZipPathToExe = Console.ReadLine().Trim(charsToTrim);
                        break;
                    }
                }
            }

            //
            // Get the source path.
            // Use the information to build the archive name.
            // Then, put the archive in the specified folder.
            //
            Console.WriteLine();
            Console.Write("Enter Website Path: ");
            sourcePath = Console.ReadLine().Trim(charsToTrim);

            Console.WriteLine();
            Console.WriteLine("The archive will be created in the parent directory.\nSpecify a different directory here, otherwise leave it blank:");
            parentDirectory = Console.ReadLine().Trim(charsToTrim);

            if (string.IsNullOrEmpty(parentDirectory))
            {
                FileInfo fi1 = new FileInfo(sourcePath);
                parentDirectory = fi1.DirectoryName;
            }

            DirectoryInfo di = new DirectoryInfo(sourcePath);
            websiteName = di.Name;

            zipName = DateTime.Now.ToString("yyyyMMdd-HHmm") + "-" + websiteName + ".zip";
            archiveArguments = "\"" + parentDirectory + @"\" + zipName + "\" \"" + sourcePath + "\"";

            Console.WriteLine();

            Process p = new Process();
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.FileName = _7ZipPathToExe;
            p.StartInfo.Arguments = "a -tzip " + archiveArguments;

            p.OutputDataReceived += new DataReceivedEventHandler(
            (s, e) => 
                { 
                    Console.WriteLine(e.Data); 
                }
            );
            p.ErrorDataReceived += new DataReceivedEventHandler((s, e) => { Console.WriteLine(e.Data); });

            p.Start();
            p.BeginOutputReadLine();
            p.WaitForExit();
            

            //
            // Pause before closing the window.
            //
            Console.WriteLine();
            Console.WriteLine("Thank you for archiving!");
            Console.ReadKey();
        }

        static string _7ZipExe
        {
            get
            {
                string[] _7ZipPaths = {
                    @"C:\Program Files\7-Zip\7z.exe",
                    @"C:\Program Files (x86)\7-Zip\7z.exe",
                    @"7za.exe"
                };

                foreach (var exe in _7ZipPaths)
                {
                    if (File.Exists(exe))
                    {
                        return exe;
                    }
                }

                return false.ToString();
            }
        }
    }
}
